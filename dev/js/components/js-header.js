(function () {
    $('.js-menu-open').on('click', function() {
        $('.header').addClass('menu-opened');
    });

    $('.js-menu-close').on('click', function() {
        $('.header').removeClass('menu-opened');
    });
}());