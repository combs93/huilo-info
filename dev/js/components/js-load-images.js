(function () {
    let messagesShown = 0;
    let messagesLoading = false;
    let allMessagesLoaded = false;

    function renderMessage(value) {
        let structure = '<div class="media__col">';
            structure += '<a href="javascript:void(0)" class="media__item">';
            structure += '<img src="' + value.src + '" alt="image"/>';
            structure += '<div class="media__item__overlay">';
            structure += '<div class="limitation-box"><span>18+</span></div>';
            structure += '<div class="media__item__overlay__icon">';
            structure += '<svg width="1em" height="1em" class="icon icon-show "><use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-show"></use></svg>';
            structure += '</div>';
            structure += '<div class="media__item__overlay__text">';
            structure += '<p>це жорстока реальність, <br/> готові побачити?</p>';
            structure += '</div>';
            structure += '<div class="media__item__overlay__action">';
            structure += '<button type="button" class="js-hide-overlay"><span>хочу правду</span>';
            structure += '<svg width="1em" height="1em" class="icon icon-arrow-dark "><use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-arrow-dark"></use></svg>';
            structure += '</button>';
            structure += '</div>';
            structure += '</div>';
            structure += '</a>';
            structure += '</div>';
        return structure;
    }
    
    function loadMessages(loadItems) {
        $.ajax({
            url: 's/data/image.json',
            type: 'GET',
            dataType: 'json',
            beforeSend: function() {
                $('.js-load-more-images').addClass('is-rotating');
                messagesLoading = true;
            }
        })
        .done(debounce(function (data) {
            const messages = data.slice(-messagesShown - loadItems, -messagesShown || data.length);
    
            if (data.length > messagesShown) {
                let html = '';
                $.each(messages, function (key, value) { html += renderMessage(value);})
                $('.media__inner').append(html);
                $('.js-load-more-images').removeClass('is-rotating');
                messagesShown += loadItems;
            } 
            
            console.log(data.length)
            console.log(messagesShown)
            console.log(data.length <= messagesShown)
            if (data.length <= messagesShown) {
                allMessagesLoaded = true;
                console.log('1')
                $('.js-load-more-images').addClass('is-hidden');
            }
    
            messagesLoading = false;
        }, 1000))
        .fail(function () {
        });
    }

    $('.js-load-more-images').on('click', function(){
        loadMessages(3);
    });



    // SHOW HIDDEN IMAGE
    $('body').on('click', '.js-hide-overlay', function() {
        $(this).closest('.media__item').addClass('is-opened');
    });
}());