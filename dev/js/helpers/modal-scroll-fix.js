let scrlTop;

$(window).on('scroll', function() {
    scrlTop = `${window.scrollY}px`;
});

const showDialog = function () {
    const body = document.body;
    let sizeChecker = document.createElement('div');
    let scrollWidth;

    
    sizeChecker.style.overflowY = 'scroll';
    sizeChecker.style.width = '50px';
    sizeChecker.style.height = '50px';
    body.append(sizeChecker);
    scrollWidth = sizeChecker.offsetWidth - sizeChecker.clientWidth;
    
    sizeChecker.remove();
    
    body.style.position = 'fixed';
    body.style.width = '100%';
    body.style.height = '100%';
    body.style.top = `-${scrlTop}`;
    body.style.overflow = 'hidden';
    body.style.paddingRight = scrollWidth + 'px';
    document.querySelector('.header').style.paddingRight = scrollWidth + 'px';
};
const closeDialog = function () {
    let body = document.body;
    let scrollY = body.style.top;
    body.style.position = '';
    body.style.top = '';
    body.style.width = '';
    body.style.overflow = '';
    body.style.paddingRight = '';
    document.querySelector('.header').style.paddingRight = '';
    window.scrollTo(0, parseInt(scrollY || '0') * -1);
}

$(document).ready(function() {
    $('.modal').on('show.bs.modal', function (event) {
        $('body').addClass('modal-active');
        showDialog();
    });

    $('.modal').on('shown.bs.modal', function (event) {
        $('body').removeClass('modal-open');
    });

    $('.modal').on('hide.bs.modal', function (event) {
        $('body').removeClass('modal-active');
        closeDialog();
    });
});